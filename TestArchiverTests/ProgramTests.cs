﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Threading;

namespace TestArchiver.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        [TestMethod]
        public void CompressDecompressTest()
        {
            var exeDir = Directory.GetCurrentDirectory();
            var inputFilename = Path.Combine(exeDir, "testFile.doc");
            var compressedFilename = Path.Combine(exeDir, "testFile.doc.gz");
            var decompressedFilename = Path.Combine(exeDir, "testfile2.doc");
            performTest(inputFilename, compressedFilename, decompressedFilename);
        }

        [TestMethod]
        public void CompressDecompressTest2()
        {
            var exeDir = Directory.GetCurrentDirectory();
            var inputFilename = Path.Combine(exeDir, "testFile2.pdf");
            var compressedFilename = Path.Combine(exeDir, "testFile2.pdf.gz");
            var decompressedFilename = Path.Combine(exeDir, "testfile2_2.pdf");
            performTest(inputFilename, compressedFilename, decompressedFilename);
        }

        private long getFileSize(string filename)
        {
            return new FileInfo(filename).Length;
        }

        private void performTest(string inputFilename, string compressedFilename, string decompressedFilename)
        {
            Assert.IsTrue(File.Exists(inputFilename));

            var inputFileLength = getFileSize(inputFilename);
            Program.Main(new[] { "compress", inputFilename, compressedFilename });
            Assert.IsTrue(File.Exists(compressedFilename));

            Program.Main(new[] { "decompress", compressedFilename, decompressedFilename });
            Assert.IsTrue(File.Exists(decompressedFilename));

            var outputFileLength = getFileSize(decompressedFilename);

            Assert.AreEqual(inputFileLength, outputFileLength);
        }
    }
}