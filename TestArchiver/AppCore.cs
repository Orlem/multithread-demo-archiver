﻿using TestArchiver.Abstractions.Interfaces;
using TestArchiver.Misc;

namespace TestArchiver
{
    public static class AppCore
    {
        private static IIocContainer _iocContainer;

        public static IIocContainer IocContainer
        {
            get
            {
                if (_iocContainer == null)
                {
                    throw new NotInitializedException("Core was not initialized");
                }

                return _iocContainer;
            }
        }

        public static void Initialize(IIocContainer iocContainer)
        {
            _iocContainer = iocContainer;
        }
    }
}