﻿using System;

namespace TestArchiver.Misc
{
    public class DependencyNotFoundException : Exception
    {
        public DependencyNotFoundException(string message) : base(message)
        {

        }
    }
}