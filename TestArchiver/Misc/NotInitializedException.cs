﻿using System;

namespace TestArchiver.Misc
{
    public class NotInitializedException : Exception
    {
        public NotInitializedException(string message) : base(message)
        {

        }
    }
}