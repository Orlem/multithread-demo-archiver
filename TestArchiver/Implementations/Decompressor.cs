﻿using System;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class Decompressor : BaseWorker, IDecompressor
    {
        protected override int GetChunkLength()
        {
            var lengthBytes = FileReader.ReadData(-1, 4);
            return lengthBytes.Length == 0
                ? 0 
                : BitConverter.ToInt32(lengthBytes, 0);
        }

        protected override IByteProcessor GetByteProcessor()
        {
            return IocContainer.ResolveDependency<IByteDecompressor>();
        }
    }
}