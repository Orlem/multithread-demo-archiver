﻿using System.Collections.Generic;
using System.IO;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class Compressor : BaseWorker, ICompressor
    {
        protected override int GetChunkLength()
        {
            return 1024 * 1024;//1mb
        }

        protected override IByteProcessor GetByteProcessor()
        {
            return IocContainer.ResolveDependency<IByteCompressor>();
        }
    }
}