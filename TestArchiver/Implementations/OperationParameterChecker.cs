﻿using System;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class OperationParameterChecker : BaseParameterChecker, IOperationParameterChecker
    {
        private const string _compressParameter = "compress";
        private const string _decompressParameter = "decompress";
        protected override bool CheckArgumentInternal(string argument, out string failedMessage)
        {
            var retv = string.Compare(argument, _compressParameter, StringComparison.InvariantCultureIgnoreCase) == 0
                       || string.Compare(argument, _decompressParameter, StringComparison.InvariantCultureIgnoreCase) == 0;

            failedMessage = retv ? string.Empty : "Wrong operation argument: [compress] or [decompress] expected.";
            return retv;
        }
    }
}