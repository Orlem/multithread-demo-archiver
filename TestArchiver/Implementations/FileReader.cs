﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class FileReader : IFileReader
    {
        private readonly object _lockObject = new object();
        private const int _defaultBufferSize = 1024 * 1024;
        private FileStream _fileStream;
        private string _fileName;
        public void Dispose()
        {
            _fileStream?.Dispose();
        }

        public void SetFilename(string filename)
        {
            lock (_lockObject)
            {
                _fileName = filename;
            }
        }

        public byte[] ReadData(long startPosition = -1, int dataLength = -1)
        {
            lock (_lockObject)
            {
                var retv = new List<byte>();
                var stream = getFileStream();
                var readBytesTotalCount = 0;
                var bytesCountToRead = dataLength == -1 ? _defaultBufferSize : dataLength;
                var buffer = new byte[bytesCountToRead];
                if (startPosition != -1)
                {
                    stream.Seek(startPosition, SeekOrigin.Begin);
                }

                var readBytes = 0;
                while ((readBytes = stream.Read(buffer, 0, bytesCountToRead)) > 0)
                {
                    retv.AddRange(buffer.Take(bytesCountToRead));
                    readBytesTotalCount += readBytes;
                    if (readBytesTotalCount >= bytesCountToRead)
                    {
                        break;
                    }
                }

                if (bytesCountToRead > readBytesTotalCount)
                {
                    retv = retv.Take(readBytesTotalCount).ToList();
                }

                return retv.ToArray();
            }
        }

        private FileStream getFileStream()
        {
            lock (_lockObject)
            {
                if (!File.Exists(_fileName))
                {
                    throw new InvalidOperationException("Filename was not set");
                }

                return _fileStream ??
                       (_fileStream = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read));
            }
        }
    }
}