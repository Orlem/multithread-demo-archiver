﻿using System;
using System.IO;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class Logger : ILogger
    {
        private readonly object _lockObject = new object();
        private string _logFileName;
        public string LogFileName => _logFileName;

        public void SetLogFileName(string fileName)
        {
            _logFileName = fileName;
            var dir = Path.GetDirectoryName(_logFileName) ?? string.Empty;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public void WriteLogMessage(string message)
        {
            lock (_lockObject)
            {
                if (string.IsNullOrEmpty(_logFileName.Trim()))
                {
                    throw new InvalidOperationException("Log file is not set.");
                }

                message = $"{DateTime.Now}->{message}\r\n";
                File.AppendAllText(_logFileName, message);
            }
        }
    }
}