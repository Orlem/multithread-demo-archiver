﻿using System.Collections.Generic;
using System.Linq;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class DataPack : IDataPack
    {
        private readonly object _lockObject = new object();
        private readonly Queue<byte[]> _internalQueue = new Queue<byte[]>();
        private readonly Dictionary<int, byte[]> _tmpList = new Dictionary<int, byte[]>();
        private int _expectedIndex = 0;
        private int _dequeueIndex = 0;

        public bool HasData()
        {
            lock (_lockObject)
            {
                return _internalQueue.Any();
            }
        }

        public void Enqueue(byte[] data, int dataIndex)
        {
            lock (_lockObject)
            {
                if (dataIndex == _expectedIndex)
                {
                    _internalQueue.Enqueue(data);
                    _expectedIndex++;
                }
                else
                {
                    _tmpList[dataIndex] = data;
                }

                if (!_tmpList.ContainsKey(_expectedIndex))
                {
                    return;
                }

                var index = _expectedIndex;
                Enqueue(_tmpList[_expectedIndex], _expectedIndex);
                _tmpList.Remove(index);

            }
        }

        public byte[] Dequeue(out int dataIndex)
        {
            lock (_lockObject)
            {
                if (!_internalQueue.Any())
                {
                    dataIndex = -1;
                    return new byte[0];
                }

                dataIndex = _dequeueIndex++;
                return _internalQueue.Dequeue();
            }
        }
    }
}