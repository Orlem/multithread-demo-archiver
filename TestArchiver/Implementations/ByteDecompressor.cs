﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class ByteDecompressor : IByteDecompressor
    {
        public byte[] ProcessBytes(byte[] inputBytes)
        {
            inputBytes = inputBytes.ToArray();
            var processedBytes = new byte[1024 * 1024];
            using (var inputStream = new MemoryStream(inputBytes))
            {
                using (var outputStream = new MemoryStream())
                {
                    using (var zipStream = new GZipStream(inputStream, CompressionMode.Decompress, true))
                    {
                        int totalBytesRead;
                        var firstPass = true;
                        while ((totalBytesRead = zipStream.Read(processedBytes, 0, processedBytes.Length)) > 0
                               || firstPass && (totalBytesRead = zipStream.Read(processedBytes, 0, processedBytes.Length)) == 0) //for some hdd/ssd not working on first time
                        {
                            firstPass = false;
                            outputStream.Write(processedBytes, 0, totalBytesRead);
                        }
                    }

                    outputStream.Close();
                    processedBytes = outputStream.ToArray();
                    if (processedBytes.Length == 0 && inputBytes.Length != 0)
                    {
                        throw new Exception("Unable to read input data");
                    }
                }
            }

            return processedBytes;
        }
    }
}