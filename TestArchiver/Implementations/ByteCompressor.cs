﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class ByteCompressor : IByteCompressor
    {
        public byte[] ProcessBytes(byte[] inputBytes)
        {
            byte[] processedBytes;
            using (var outputStream = new MemoryStream())
            {
                using (var zipStream = new GZipStream(outputStream, CompressionMode.Compress, true))
                {
                    zipStream.Write(inputBytes, 0, inputBytes.Length);
                }

                processedBytes = outputStream.ToArray();
            }

            var lengthBytes = BitConverter.GetBytes(processedBytes.Length); //added 4 bytes
            return lengthBytes.Concat(processedBytes).ToArray();
        }
    }
}