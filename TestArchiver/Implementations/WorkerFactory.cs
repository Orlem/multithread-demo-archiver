﻿using System;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class WorkerFactory : BaseEntity, IWorkerFactory
    {
        public IWorker GetWorker(string parameter)
        {
            var retv = string.Compare(parameter, "compress", StringComparison.InvariantCultureIgnoreCase) == 0
                ? (IWorker)IocContainer.ResolveDependency<ICompressor>()
                : (IWorker)IocContainer.ResolveDependency<IDecompressor>();

            return retv;
        }
    }
}