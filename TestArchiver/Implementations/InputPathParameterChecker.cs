﻿using System.IO;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class InputPathParameterChecker : BaseParameterChecker, IInputPathParameterChecker
    {
        protected override bool CheckArgumentInternal(string argument, out string failedMessage)
        {
            var retv = File.Exists(argument);
            failedMessage = retv ? string.Empty : "Input file does not exist.";
            return retv;
        }
    }
}