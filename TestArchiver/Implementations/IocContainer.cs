﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TestArchiver.Abstractions.Interfaces;
using TestArchiver.Misc;

namespace TestArchiver.Implementations
{
    public class IocContainer : IIocContainer
    {
        private readonly Dictionary<Type, Type> _dependenciesDictionary = new Dictionary<Type, Type>();
        public void RegisterDependency<TAbstraction, TImplementation>() where TAbstraction : class where TImplementation : class, TAbstraction
        {
            _dependenciesDictionary[typeof(TAbstraction)] = typeof(TImplementation);
        }

        public TAbstraction ResolveDependency<TAbstraction>() where TAbstraction : class
        {
            if (!_dependenciesDictionary.ContainsKey(typeof(TAbstraction)))
            {
                throw new DependencyNotFoundException($"Unable to resolve dependency. Abstraction type is {typeof(TAbstraction).FullName}");
            }

            var implementationType = _dependenciesDictionary[typeof(TAbstraction)];
            var retv = Activator.CreateInstance(implementationType) as TAbstraction;
            injectIocContainer(retv);
            return retv;
        }

        private void injectIocContainer(object createdInstance)
        {
            var fields = getFields(createdInstance.GetType());
            foreach (var field in fields.Where(f => f.FieldType == typeof(IIocContainer)))
            {
                field.SetValue(createdInstance, this);
            }
        }

        private IEnumerable<FieldInfo> getFields(Type objectType)
        {
            var retv = objectType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            if (objectType.BaseType == typeof(object) || objectType.BaseType == null)
            {
                return retv;
            }

            var baseFields = getFields(objectType.BaseType);
            retv.AddRange(baseFields);

            return retv;
        }
    }
}