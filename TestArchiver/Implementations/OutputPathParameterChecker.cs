﻿using System.IO;
using System.Security;
using System.Security.Permissions;
using TestArchiver.Abstractions;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class OutputPathParameterChecker : BaseParameterChecker, IOutputPathParameterChecker
    {
        protected override bool CheckArgumentInternal(string argument, out string failedMessage)
        {
            var outDir = Path.GetDirectoryName(argument) ?? string.Empty;
            var retv = Directory.Exists(outDir);
            failedMessage = retv ? string.Empty : "Output directory does not exist";
            if (!retv)
            {
                return retv;
            }

            retv = checkCanWrite(argument);
            if (!retv)
            {
                failedMessage = $"Unable to create file {argument}. Access is denied.";
            }

            return retv;
        }

        private bool checkCanWrite(string filePath)
        {
            var writePermission = new FileIOPermission(FileIOPermissionAccess.Write, filePath);
            return SecurityManager.IsGranted(writePermission);
        }
    }
}