﻿using System;
using System.IO;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Implementations
{
    public class FileWriter : IFileWriter
    {
        private readonly object _lockObject = new object();
        private FileStream _fileStream;
        private string _fileName;

        public void Dispose()
        {
            _fileStream?.Dispose();
        }

        public long CurrentPosition => getFileStream().Position;
        public int WriteData(byte[] data)
        {
            lock (_lockObject)
            {
                var stream = getFileStream();
                stream.Write(data, 0, data.Length);
            }

            return data.Length;
        }

        public void SetFilename(string filename)
        {
            _fileName = filename;
        }

        private FileStream getFileStream()
        {
            lock (_lockObject)
            {
                return _fileStream ??
                       (_fileStream = new FileStream(_fileName, FileMode.Create, FileAccess.Write, FileShare.Read));
            }
        }
    }
}