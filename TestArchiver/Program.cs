﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestArchiver.Abstractions.Interfaces;
using TestArchiver.Implementations;

namespace TestArchiver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                showUsageHint();
                return;
            }

            initializerApp();
            var arg0Checker = AppCore.IocContainer.ResolveDependency<IOperationParameterChecker>();
            var arg1Checker = AppCore.IocContainer.ResolveDependency<IInputPathParameterChecker>();
            var arg2Checker = AppCore.IocContainer.ResolveDependency<IOutputPathParameterChecker>();

            var argsToCheckersDict = new Dictionary<string, IArgumentChecker>
            {
                { args[0], arg0Checker },
                { args[1], arg1Checker },
                { args[2], arg2Checker },
            };

            if (!checkArgsAreOk(argsToCheckersDict))
            {
                var failedArgCheckers = argsToCheckersDict.Values.Where(x => !x.ArgumentIsOk);
                displayFailedArgsMessages(failedArgCheckers);
                showUsageHint();
                return;
            }

            var workerFactory = AppCore.IocContainer.ResolveDependency<IWorkerFactory>();
            using (var worker = workerFactory.GetWorker(args[0]))
            {
                var logger = getLogger();
                var result = runWorkerSafe(worker, args, logger);
                displayWorkResult(result, logger);
            }
        }

        private static bool runWorkerSafe(IWorker worker, string[] args, ILogger logger)
        {
            try
            {
                worker.StartProcess(args[1], args[2], logger);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Unexpected error occured: {e.Message}\r\n{e.StackTrace}");
            }

            return true;
        }

        private static ILogger getLogger()
        {
            var retv = AppCore.IocContainer.ResolveDependency<ILogger>();
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var fileName = Path.Combine(appDataPath, $"archiver_{DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-fff")}.log");
            retv.SetLogFileName(fileName);
            return retv;
        }

        private static void displayWorkResult(bool result, ILogger logger)
        {
            var message = result ? "Operation OK" : "Operation failed";
            message = $"{message}\r\nLog: {logger.LogFileName}";
            Console.WriteLine(message);
        }

        private static void displayFailedArgsMessages(IEnumerable<IArgumentChecker> failedArgumentCheckers)
        {
            foreach (var failedArgumentChecker in failedArgumentCheckers)
            {
                Console.WriteLine(failedArgumentChecker.CheckFailedMessage);
            }
        }

        private static bool checkArgsAreOk(Dictionary<string, IArgumentChecker> argsToCheckersDict)
        {
            return argsToCheckersDict.All(pair => pair.Value.CheckArgument(pair.Key));
        }

        private static void showUsageHint()
        {
            Console.WriteLine("Usage hint:\r\nTestArchiver.exe compress/decompress <input file name> <output file name>");
        }

        private static void initializerApp()
        {
            var iocContainer = new IocContainer();
            iocContainer.RegisterDependency<IWorkerFactory, WorkerFactory>();
            iocContainer.RegisterDependency<ICompressor, Compressor>();
            iocContainer.RegisterDependency<IDecompressor, Decompressor>();
            iocContainer.RegisterDependency<IOperationParameterChecker, OperationParameterChecker>();
            iocContainer.RegisterDependency<IInputPathParameterChecker, InputPathParameterChecker>();
            iocContainer.RegisterDependency<IOutputPathParameterChecker, OutputPathParameterChecker>();
            iocContainer.RegisterDependency<ILogger, Logger>();
            iocContainer.RegisterDependency<IFileReader, FileReader>();
            iocContainer.RegisterDependency<IFileWriter, FileWriter>();
            iocContainer.RegisterDependency<IByteCompressor, ByteCompressor>();
            iocContainer.RegisterDependency<IByteDecompressor, ByteDecompressor>();
            iocContainer.RegisterDependency<IDataPack, DataPack>();

            AppCore.Initialize(iocContainer);
        }
    }
}
