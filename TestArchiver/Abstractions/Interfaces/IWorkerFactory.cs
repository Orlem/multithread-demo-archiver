﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface IWorkerFactory
    {
        IWorker GetWorker(string parameter);
    }
}