﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface ILogger
    {
        string LogFileName { get; }
        void SetLogFileName(string fileName);
        /// <summary>
        /// Writes message to log. Thread safe
        /// </summary>
        /// <param name="message"></param>
        void WriteLogMessage(string message);
    }
}