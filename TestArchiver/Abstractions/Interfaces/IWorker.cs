﻿using System;

namespace TestArchiver.Abstractions.Interfaces
{
    public interface IWorker : IDisposable
    {
        bool StartProcess(string inputFile, string outputFile, ILogger logger);
    }
}