﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface IByteProcessor
    {
        byte[] ProcessBytes(byte[] inputBytes);
    }
}