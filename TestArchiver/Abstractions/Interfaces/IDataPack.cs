﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface IDataPack
    {
        bool HasData();
        void Enqueue(byte[] data, int dataIndex);
        byte[] Dequeue(out int dataIndex);
    }
}