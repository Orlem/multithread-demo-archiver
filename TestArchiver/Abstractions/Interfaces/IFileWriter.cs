﻿using System;

namespace TestArchiver.Abstractions.Interfaces
{
    public interface IFileWriter : IDisposable
    {
        long CurrentPosition { get; }

        /// <summary>
        /// Write data to file, thread safe
        /// </summary>
        /// <param name="data">Bytes to write</param>
        /// <returns>Block position</returns>
        int WriteData(byte[] data);

        void SetFilename(string filename);
    }
}