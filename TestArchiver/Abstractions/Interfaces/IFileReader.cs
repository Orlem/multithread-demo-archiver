﻿using System;

namespace TestArchiver.Abstractions.Interfaces
{
    public interface IFileReader : IDisposable
    {
        void SetFilename(string filename);
        /// <summary>
        /// Read data, thread safe
        /// </summary>
        /// <param name="startPosition">Start position, -1 - continue reading from current position</param>
        /// <param name="dataLength">Data length. -1 - read to end</param>
        /// <returns></returns>
        byte[] ReadData(long startPosition = -1, int dataLength = -1);
    }
}