﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface IIocContainer
    {
        void RegisterDependency<TAbstraction, TImplementation>() where TImplementation : class, TAbstraction where TAbstraction : class;
        TAbstraction ResolveDependency<TAbstraction>() where TAbstraction : class;
    }
}