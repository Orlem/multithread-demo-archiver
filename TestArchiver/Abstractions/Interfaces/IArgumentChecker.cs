﻿namespace TestArchiver.Abstractions.Interfaces
{
    public interface IArgumentChecker
    {
        bool ArgumentIsOk { get; }
        string CheckFailedMessage { get; }
        bool CheckArgument(string argument);
    }
}