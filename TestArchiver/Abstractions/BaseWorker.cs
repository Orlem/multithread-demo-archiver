﻿using System;
using System.Linq;
using System.Threading;
using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Abstractions
{
    public abstract class BaseWorker : BaseEntity, IWorker
    {
        private readonly object _failedFlagLocker = new object();
        private readonly object _readingLocker = new object();
        private readonly object _processingLocker = new object();
        private IFileReader _fileReader;
        private IFileWriter _fileWriter;
        private ILogger _logger;
        private readonly ManualResetEvent _readNewChunkEvent = new ManualResetEvent(false);
        private readonly AutoResetEvent _processedNewChunkEvent = new AutoResetEvent(false);
        protected IFileReader FileReader => _fileReader;

        private IDataPack _rawDataPack;
        private IDataPack _processedDataPack;

        private bool _isReading;
        private bool _isProcessing;
        private bool _failedFlag;

        private Thread _readingThread;
        private Thread _writingThread;
        private Thread[] _processThreads;

        private int _processCounter;

        private void prepareProcess(string inputFile, string outputFile)
        {
            _rawDataPack = IocContainer.ResolveDependency<IDataPack>();
            _processedDataPack = IocContainer.ResolveDependency<IDataPack>();

            _fileReader = IocContainer.ResolveDependency<IFileReader>();
            _fileReader.SetFilename(inputFile);

            _fileWriter = IocContainer.ResolveDependency<IFileWriter>();
            _fileWriter.SetFilename(outputFile);
        }

        public bool StartProcess(string inputFile, string outputFile, ILogger logger)
        {
            _logger = logger;
            prepareProcess(inputFile, outputFile);
            handleThreads();

            _readingThread.Join();

            _isReading = false;
            _readNewChunkEvent.Set();

            foreach (var processThread in _processThreads)
            {
                processThread.Join();
            }

            _isProcessing = false;
            _processedNewChunkEvent.Set();

            _writingThread.Join();

            return !_failedFlag;
        }

        private void handleThreads()
        {
            _readingThread = new Thread(readData);
            _isReading = true;
            _readingThread.Start();

            _processThreads = new Thread[Environment.ProcessorCount - 2];
            _isProcessing = true;
            for (var i = 0; i < _processThreads.Length; i++)
            {
                _processThreads[i] = new Thread(processData);
                _processThreads[i].Start();
            }

            _writingThread = new Thread(writeData);
            _writingThread.Start();
        }

        private void readData()
        {
            try
            {
                var blockIndex = 0;
                while (!checkFailed())
                {
                    var bytesToRead = GetChunkLength();
                    var rawData = bytesToRead > 0
                        ? _fileReader.ReadData(-1, bytesToRead)
                        : new byte[0];

                    if (!rawData.Any())
                    {
                        return;
                    }

                    _rawDataPack.Enqueue(rawData, blockIndex);
                    _logger.WriteLogMessage($"Read block #{blockIndex}, Size: {bytesToRead} bytes");
                    blockIndex++;

                    _readNewChunkEvent.Set();
                }
            }
            catch (Exception e)
            {
                setBoolValueThreadSafe(v => _failedFlag = v, true, _failedFlagLocker);
                _logger.WriteLogMessage($"Read data error: {e.Message}\r\n{e.StackTrace}");
            }
        }

        private void processData()
        {
            try
            {
                while (!checkFailed())
                {
                    int dataIndex;
                    var rawData = _rawDataPack.Dequeue(out dataIndex);
                    while (dataIndex == -1 && checkBoolValueThreadSafe(() => _isReading, _readingLocker))
                    {
                        _readNewChunkEvent.WaitOne();
                        rawData = _rawDataPack.Dequeue(out dataIndex);
                    }

                    if (dataIndex == -1)
                    {
                        return;
                    }

                    var processedData = GetByteProcessor().ProcessBytes(rawData);
                    _processedDataPack.Enqueue(processedData, dataIndex);
                    _logger.WriteLogMessage($"Processed block #{dataIndex}, Size: {processedData.Length} bytes");

                    _processedNewChunkEvent.Set();
                }
            }
            catch (Exception e)
            {
                setBoolValueThreadSafe(v => _failedFlag = v, true, _failedFlagLocker);
                _logger.WriteLogMessage($"Process data error: {e.Message}\r\n{e.StackTrace}");
            }
        }

        private void writeData()
        {
            try
            {
                while (!checkFailed())
                {
                    int dataIndex;
                    var processedData = _processedDataPack.Dequeue(out dataIndex);
                    while (dataIndex == -1 && checkBoolValueThreadSafe(() => _isProcessing, _processingLocker))
                    {
                        _readNewChunkEvent.WaitOne();
                        processedData = _processedDataPack.Dequeue(out dataIndex);
                    }

                    if (dataIndex == -1)
                    {
                        return;
                    }

                    _fileWriter.WriteData(processedData);
                    _logger.WriteLogMessage($"Written block #{dataIndex}, Size: {processedData.Length} bytes");
                }
            }
            catch (Exception e)
            {
                setBoolValueThreadSafe(v => _failedFlag = v, true, _failedFlagLocker);
                _logger.WriteLogMessage($"Write data error: {e.Message}\r\n{e.StackTrace}");
            }
        }

        private bool checkFailed()
        {
            return checkBoolValueThreadSafe(() => _failedFlag, _failedFlagLocker);
        }

        private bool checkBoolValueThreadSafe(Func<bool> getValueFunc, object locker)
        {
            lock (locker)
            {
                return getValueFunc.Invoke();
            }
        }

        private void setBoolValueThreadSafe(Action<bool> setValueAction, bool value, object locker)
        {
            lock (locker)
            {
                setValueAction?.Invoke(value);
            }
        }

        protected abstract int GetChunkLength();

        protected abstract IByteProcessor GetByteProcessor();

        public void Dispose()
        {
            _fileReader?.Dispose();
            _fileWriter?.Dispose();
        }
    }

}