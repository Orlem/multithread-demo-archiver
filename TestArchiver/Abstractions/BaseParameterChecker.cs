﻿using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Abstractions
{
    public abstract class BaseParameterChecker : IArgumentChecker
    {
        public bool ArgumentIsOk { get; private set; }
        public string CheckFailedMessage { get; private set; }
        public bool CheckArgument(string argument)
        {
            string failedMessage;
            var retv = CheckArgumentInternal(argument, out failedMessage);
            CheckFailedMessage = failedMessage;
            ArgumentIsOk = retv;
            return retv;
        }

        protected abstract bool CheckArgumentInternal(string argument, out string failedMessage);
    }
}