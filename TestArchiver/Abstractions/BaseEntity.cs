﻿using TestArchiver.Abstractions.Interfaces;

namespace TestArchiver.Abstractions
{
    public abstract class BaseEntity
    {
        //injectable 
        private IIocContainer _iocContainer;
        protected IIocContainer IocContainer => _iocContainer;
    }
}