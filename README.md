# Multithread Demo Archiver
The objective is to create console multithread archiver using GZipStream, .Net Framework 3.5, using Threads and syncronization primitives only.
Usage:
 - compression: testarchiver.exe compress <input file> <output compressed file>
 - decompression: testarchiver.exe decompress <input compressed file> <output file>